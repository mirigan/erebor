# Erebor

This is a test side project. I try new rails features, gems, code pattern, system make dev easier or gitlab features
Like :

- Rails dockerfile
- Stimulus / Turbo
- Gitlab Pipelines
- Gem Faraday
- Gem ViewComponents

I name it erebor just because I like the Lord Of The Ring and the especially Dwarfs
I also use this Readme file to store ressources links and some things to do or try outside this project

## Things that I want to test/do

- [ ] Dig more into the component UI and take a look to [Lookbook](https://github.com/ViewComponent/lookbook)
- [ ] Try the gem [Noticed](https://github.com/excid3/noticed) to send notification, email, etc...
- [ ] [Simple Icon](https://github.com/simple-icons/simple-icons) insteed of Heroicon
- [ ] Use [Sentry](https://sentry.io/for/rails/) to check errors
- [ ] [PicoCSS](https://github.com/picocss/picocss.com) appraoch of CSS classes
- [ ] A system to create a documentation like [https://docsify.js.org](https://docsify.js.org)
- [ ] A system to create a changelog like [git-cliff](https://github.com/orhun/git-cliff)
- [ ] Build my own authentication system with `generates_token_for` [Doc](https://edgeapi.rubyonrails.org/classes/ActiveRecord/TokenFor/ClassMethods.html) | [Saeloun Article](https://blog.saeloun.com/2023/11/14/rails-7-1-introduces-active-record-generate-token-for/) | [Rails blog Article](https://rubyonrails.org/2023/10/5/Rails-7-1-0-has-been-released)
- [ ] Look into [Current Attributes](https://api.rubyonrails.org/classes/ActiveSupport/CurrentAttributes.html)
- [ ] Not for now but, try an other templating lang [Haml](https://github.com/haml/haml)
- [ ] Use framasoft page errors images [Link](https://http.framasoft.org/)

### Infra or Stack

- [ ] [Kamal](https://kamal-deploy.org/), but I don't have any server for the moment
- [ ] With Kamal I want to try the [Gitlab Environment and deployments](https://docs.gitlab.com/ee/ci/environments/index.html#view-environments-and-deployments)
- [ ] Try the open source infrastructure as code tool [Opentofu](https://opentofu.org/)
- [ ] Try an alternative to redis with [Dragonflydb](https://github.com/dragonflydb/dragonfly)

### Done

- [x] Take look to [Overmind & Hivemind](https://evilmartians.com/chronicles/introducing-overmind-and-hivemind)

### Not Ruby/Rails things

### Done

- [x] I write my resume with [Rx Resume](https://rxresu.me) | [Link to my resume](https://rxresu.me/mirigan/chronic-passive-capybara)

## Ressources

- [Rails Blog](https://rubyonrails.org/blog/)
- [RailsNotes](https://railsnotes.xyz/)
- [Boring Rails](https://boringrails.com/)
- [Go Rails](https://gorails.com/)
- [Evil Martians](https://evilmartians.com/chronicles)
- [AppSignal Blog](https://blog.appsignal.com/category/ruby.html)
- [Saeloun Blog](https://blog.saeloun.com/)
- [code.avi.nyc](https://code.avi.nyc/)
- I also use the web extension [daily.dev](https://daily.dev/) for daily news

## Informations about this project

- Ruby version : 3.3.0

- System dependencies : bundler

  - I use Depfu to check dependencies, rails and ruby updates

- Database : PostgreSQL

- How to run the test suite : rspec

- How to run dev :

  - First check if you have [tmux](https://github.com/tmux/tmux/wiki/Installing) and [overmind](https://github.com/DarthSim/overmind) installed
  - run `./bin/dev`
