# frozen_string_literal: true

RSpec.describe Identity::EmailsController do
  before do
    sign_in_as(create(:user))
  end

  describe 'GET /identity/email' do
    it 'gets edit' do
      get edit_identity_email_url
      assert_response :success
    end
  end

  describe 'PATCH /identity/email' do
    it 'updates email' do
      patch identity_email_url, params: { email: 'new_email@hey.com', password_challenge: 'Secret1*3*5*' }
      assert_redirected_to root_url
    end

    context 'with wrong password challenge' do
      it 'does not update email' do
        patch identity_email_url, params: { email: 'new_email@hey.com', password_challenge: 'SecretWrong1*3' }

        assert_response :unprocessable_entity
        assert_select 'li', /Password challenge is invalid/
      end
    end
  end
end
