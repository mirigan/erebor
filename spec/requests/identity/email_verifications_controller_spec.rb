# frozen_string_literal: true

RSpec.describe Identity::EmailVerificationsController do
  describe 'POST /identity/email_verification' do
    let(:user) { sign_in_as(create(:user, verified: false)) }

    it 'sends a verification email' do
      assert_enqueued_email_with UserMailer, :email_verification, params: { user: } do
        post identity_email_verification_url
      end

      assert_redirected_to root_url
    end
  end

  describe 'GET /identity/email_verification' do
    let(:user) { sign_in_as(create(:user, verified: false)) }
    let!(:sid) { user.generate_token_for(:email_verification) }

    it 'verifies email' do
      get identity_email_verification_url(sid:, email: user.email)
      assert_redirected_to root_url
    end

    context 'with expired token' do
      it 'does not verify email' do
        travel 3.days

        get identity_email_verification_url(sid:, email: user.email)

        assert_redirected_to edit_identity_email_url
        expect(flash[:alert]).to eq('That email verification link is invalid')
      end
    end
  end
end
