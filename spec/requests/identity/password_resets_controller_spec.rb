# frozen_string_literal: true

RSpec.describe Identity::PasswordResetsController do
  before do
    sign_in_as(create(:user, verified: true))
  end

  describe 'GET /identity/password_reset/new' do
    it 'gets new' do
      get new_identity_password_reset_url
      assert_response :success
    end
  end

  describe 'GET /identity/password_reset/edit' do
    let(:user) { User.first }
    let(:sid) { user.generate_token_for(:password_reset) }

    it 'gets edit' do
      get edit_identity_password_reset_url(sid:)
      assert_response :success
    end
  end

  describe 'POST /identity/password_reset' do
    let(:user) { User.first }

    it 'sends a password reset email' do
      assert_enqueued_email_with UserMailer, :password_reset, params: { user: } do
        post identity_password_reset_url, params: { email: user.email }
      end

      assert_redirected_to sign_in_url
    end

    context 'with a nonexistent email' do
      it 'does not send a password reset email' do
        assert_no_enqueued_emails do
          post identity_password_reset_url, params: { email: 'invalid_email@hey.com' }
        end

        assert_redirected_to new_identity_password_reset_url
        expect(flash[:alert]).to eq("You can't reset your password until you verify your email")
      end
    end

    context 'with a unverified email' do
      it 'does not send a password reset email' do # rubocop:disable RSpec/ExampleLength
        user.update! verified: false

        assert_no_enqueued_emails do
          post identity_password_reset_url, params: { email: user.email }
        end

        assert_redirected_to new_identity_password_reset_url
        expect(flash[:alert]).to eq("You can't reset your password until you verify your email")
      end
    end
  end

  describe 'PATCH /identity/password_reset' do
    let(:user) { User.first }
    let!(:sid) { user.generate_token_for(:password_reset) }

    it 'updates password' do
      patch identity_password_reset_url,
            params: { sid:, password: 'Secret6*4*2*', password_confirmation: 'Secret6*4*2*' }
      assert_redirected_to sign_in_url
    end

    context 'with expired token' do
      it 'does not update password' do
        travel 30.minutes

        patch identity_password_reset_url,
              params: { sid:, password: 'Secret6*4*2*', password_confirmation: 'Secret6*4*2*' }

        assert_redirected_to new_identity_password_reset_url
        expect(flash[:alert]).to eq('That password reset link is invalid')
      end
    end
  end
end
