# frozen_string_literal: true

RSpec.describe SessionsController do
  describe 'GET /sessions' do
    let(:user) { create(:user) }

    it 'gets index' do
      sign_in_as user

      get sessions_url
      assert_response :success
    end
  end

  describe 'GET /sign_in' do
    it 'gets new' do
      get sign_in_url
      assert_response :success
    end
  end

  describe 'POST /sign_in' do
    let(:user) { create(:user) }

    it 'signs in' do
      post sign_in_url, params: { email: user.email, password: 'Secret1*3*5*' }
      assert_redirected_to root_url
    end

    context 'with wrong credentials' do
      it 'does not sign in' do
        post sign_in_url, params: { email: user.email, password: 'SecretWrong1*3' }
        assert_redirected_to sign_in_url(email_hint: user.email)
        expect(flash[:alert]).to eq('That email or password is incorrect')
      end
    end
  end

  describe 'DELETE /sessions/:id' do
    let(:user) { create(:user) }

    it 'signs out' do
      sign_in_as user

      delete session_url(user.sessions.last)
      assert_redirected_to sessions_url

      follow_redirect!
      assert_redirected_to sign_in_url
    end
  end
end
