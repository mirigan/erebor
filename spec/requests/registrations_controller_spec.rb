# frozen_string_literal: true

RSpec.describe RegistrationsController do
  describe 'GET /sign_up' do
    let(:user) { create(:user) }

    it 'gets new' do
      get sign_up_url
      assert_response :success
    end
  end

  describe 'POST /sign_up' do
    let(:params) do
      { email: 'lazaronixon@hey.com', username: Faker::Internet.username, password: 'Secret1*3*5*',
        password_confirmation: 'Secret1*3*5*' }
    end

    it 'signs up' do
      assert_difference('User.count') do
        post sign_up_url,
             params:
      end

      assert_redirected_to root_url
    end
  end
end
