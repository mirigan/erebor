# frozen_string_literal: true

RSpec.describe PasswordsController do
  before do
    @user = sign_in_as(create(:user))
  end

  describe 'GET /password/edit' do
    it 'gets edit' do
      get edit_password_url
      assert_response :success
    end
  end

  describe 'PATCH /password/edit' do
    it 'updates password' do
      patch password_url,
            params: { password_challenge: 'Secret1*3*5*', password: 'Secret6*4*2*',
                      password_confirmation: 'Secret6*4*2*' }
      assert_redirected_to root_url
    end

    context 'with wrong password challenge' do
      it 'does not update password' do
        patch password_url,
              params: { password_challenge: 'SecretWrong1*3', password: 'Secret6*4*2*',
                        password_confirmation: 'Secret6*4*2*' }

        assert_response :unprocessable_entity
        assert_select 'li', /Password challenge is invalid/
      end
    end
  end
end
