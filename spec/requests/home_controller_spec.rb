# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HomeController do
  describe 'GET /' do
    it 'renders the index template' do
      get '/'

      expect(response).to render_template(:index)
    end
  end
end
