# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserMailer do
  describe '#password_reset' do
    let(:user) { create(:user) }
    let(:mail) { described_class.with(user:).password_reset }

    it 'sends password reset mail' do
      expect(mail.subject).to eq('Reset your password')
      expect(mail.to).to eq([user.email])
    end
  end

  describe '#email_verification' do
    let(:user) { create(:user) }
    let(:mail) { described_class.with(user:).email_verification }

    it 'sends email verification mail' do
      expect(mail.subject).to eq('Verify your email')
      expect(mail.to).to eq([user.email])
    end
  end
end
