# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Atoms::Button::Component, type: :component do
  it 'render button as default' do
    expect(
      render_inline(described_class.new(label: 'label')).css('button').to_html
    ).to include('label')
  end

  it 'render a when href' do
    render_inline(described_class.new(label: 'link', href: '/')).css('a').to_html

    expect(page).to have_link 'link', href: '/'
  end

  it 'render i when icon' do
    render_inline(described_class.new(label: 'link', icon: 'user-circle')).css('button').to_html

    expect(page).to have_css 'i', class: 'ph-bold ph-user-circle'
  end
end
