# frozen_string_literal: true

# require 'capybara/cuprite'

# Capybara.javascript_driver = :cuprite
# Capybara.register_driver(:cuprite) do |app|
#   Capybara::Cuprite::Driver.new(app, window_size: [1200, 800])
# end

# RSpec.configure do |config|
#   config.include Helpers::System::Authentication, type: :system

#   config.before(:each, type: :system) do
#     driven_by(:cuprite)
#   end
# end
