# frozen_string_literal: true

module Helpers
  module System
    module Authentication
      def sign_in_as(user, password = 'Secret1*3*5*')
        visit sign_in_url
        fill_in :email, with: user.email
        fill_in :password, with: password
        click_on 'Sign in'

        assert_current_path root_url
        user
      end
    end
  end
end
