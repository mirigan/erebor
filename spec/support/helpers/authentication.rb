# frozen_string_literal: true

module Helpers
  module Authentication
    def sign_in_as(user, password = 'Secret1*3*5*')
      post(sign_in_path, params: { email: user.email, password: })
      user
    end
  end
end
