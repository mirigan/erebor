# frozen_string_literal: true

module Atoms
  module Input
    class Component < ApplicationViewComponent
      def initialize(object_name: nil, form_for_type: false, form: nil, **options)
        super
        @object_name = object_name
        @form_for_type = form_for_type
        @f = form
        @options = options.merge(needed_options) { |_key, new, old| "#{old} #{new}" }
      end

      private

      def needed_options
        {
          class: 'input'
        }
      end
    end
  end
end
