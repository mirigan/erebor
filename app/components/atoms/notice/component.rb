# frozen_string_literal: true

module Atoms
  module Notice
    class Component < ApplicationViewComponent
      def initialize(text:, style: 'primary', href: false, closable: true)
        super
        @text = text
        @style = style
        @href = href
        @closable = closable

        raise ::ArgumentError, 'text MUST be present' if @text.blank?
      end

      private

      def theme
        themes = {
          primary: 'notice-primary',
          success: 'notice-success',
          warning: 'notice-warning'
        }

        themes[@style]
      end
    end
  end
end
