import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  close(event) {
    event.target.closest('article.notice').remove();
  }
}
