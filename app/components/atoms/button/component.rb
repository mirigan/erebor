# frozen_string_literal: true

module Atoms
  module Button
    class Component < ApplicationViewComponent
      def initialize(label:, style: 'primary', href: false, icon: false, **options)
        super
        @label = label
        @style = style
        @href = href
        @icon = icon
        @options = options.merge(needed_options) { |_key, new, old| "#{old} #{new}" }

        raise ArgumentError, 'label MUST be present' if @label.blank?
      end

      private

      def theme
        case @style
        when 'primary'
          'btn-primary'
        when 'secondary'
          'btn-secondary'
        end
      end

      def needed_options
        {
          class: theme
        }
      end
    end
  end
end
