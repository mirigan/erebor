# frozen_string_literal: true

class ApplicationViewComponent < ViewComponent::Base
  include ApplicationHelper

  private

  def identifier
    @identifier ||= self.class.name.underscore.split('/').join('--').gsub('_', '-')
  end

  def class_for(name, from: identifier)
    "c-#{from}-#{name}"
  end
end
